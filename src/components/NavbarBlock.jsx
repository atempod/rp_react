import React, { Component } from "react";
import {
  NavLink
  // Link
} from "react-router-dom";
import classes from "./NavbarBlock.css";

const links = [
  //   { to: "/", label: "Home Page", exact: true },
  { to: "/filtered-vacancies", label: "Ищу работу", exact: false },
  { to: "/init", label: "Разместить вакансию", exact: false },
  { to: "/one-more", label: "Разместить вакансию", exact: false },
  { to: "/www", label: "Контакты", exact: false }
];

class NavbarBlock extends Component {
  // state = {};
  renderLinks() {
    return links.map((link, index) => {
      return (
        <li key={index}>
          <NavLink
            to={link.to}
            exact={link.exact}
            activeClassName={classes.active}
          >
            {link.label}
          </NavLink>
        </li>
      );
    });
  }
  render() {
    const csl = [classes.NavbarBlock];
    return (
      <nav className={csl.join(" ")}>
        <ul>{this.renderLinks()}</ul>
      </nav>
      //   <nav className={csl.join(" ")}>
      //     <ul>
      //       <li>
      //         <NavLink to="/init">Ищу работу</NavLink>
      //       </li>

      //       <li>
      //         <NavLink to="/">Разместить вакансию</NavLink>
      //       </li>
      //       <li>
      //         <NavLink to="/emps">Ищу сотрудников</NavLink>
      //       </li>
      //       <li>
      //         <NavLink to="/contacts">Контакты</NavLink>
      //       </li>
      //     </ul>
      //   </nav>
    );
  }
}

export default NavbarBlock;
