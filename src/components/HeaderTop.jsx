import React from "react";
import { Container } from "reactstrap";
import classes from "./HeaderTop.css";

class HeaderTop extends React.Component {
  render() {
    return (
      <header className={"bg-rpGreySuperLight"}>
        <Container>
          <div className={classes.TopHeaderBar}>
            <span className={classes.TopHeaderSpanLeft}>
              Город не определен (Страна не определена)
            </span>
            <span className={classes.TopHeaderSpanRight}>+79250502726</span>
          </div>
        </Container>
      </header>
    );
  }
}

export default HeaderTop;
