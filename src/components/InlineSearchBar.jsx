import React from "react";

const InlineSearchBar = () => {
  return (
    <form>
      <input type="text" placeholder="Search..." />
      <p>
        <input type="checkbox" /> Only show active vacancies
      </p>
    </form>
  );
};

export default InlineSearchBar;
