import React from "react";
import classes from "./InputRP.css";

function isInvalid({ name, valid, touched, shouldValidate }) {
  return !valid && shouldValidate && touched;
}

const InputRP = props => {
  const inputType = props.type || "text";
  const cls = [classes.InputRP, classes[props.inputStyle]];
  const htmlFor = `${inputType}-${Math.random()}`;

  return (
    <div className={cls.join(" ")}>
      <div>
        <input
          type={props.type}
          id={htmlFor}
          value={props.value}
          onChange={props.onChange}
          placeholder={props.inputPlaceholder}
        />
      </div>
      <div className={classes.errorMessage}>
        {isInvalid(props) ? (
          <span>{props.errorMessage || "Введите верное значение"}</span>
        ) : null}
      </div>
    </div>
  );
};

export default InputRP;
