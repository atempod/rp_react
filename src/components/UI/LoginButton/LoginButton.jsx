import React from "react";
import classes from "./LoginButton.css";

const LoginButton = props => {
  const cls = [
    classes.Button
    // classes[props.type]
  ];
  console.log("LoginButton props", props);
  return (
    <React.Fragment>
      <button onClick={props.onLoginOpen} className={cls.join(" ")}>
        {props.children}
      </button>
    </React.Fragment>
  );
};

export default LoginButton;
