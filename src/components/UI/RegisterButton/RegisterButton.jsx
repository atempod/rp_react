import React from "react";
import classes from "./RegisterButton.css";

const RegisterButton = props => {
  const cls = [
    classes.Button
    // classes[props.type]
  ];
  console.log("RegisterButton props", props);
  return (
    <React.Fragment>
      <button
        disabled={props.disabled}
        onClick={props.onRegisterOpen}
        className={cls.join(" ")}
      >
        {props.children}
      </button>
    </React.Fragment>
  );
};

export default RegisterButton;
