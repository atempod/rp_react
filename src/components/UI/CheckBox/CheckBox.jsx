import React, { Component } from "react";
import "./CheckBox.css";

class Checkbox extends Component {
  render() {
    return (
      <div className="checkbox">
        {/* <input type="checkbox" name="thing" value="valuable" id="thing" />
        <label for="thing" /> {this.props.text} */}
        <input
          type="checkbox"
          name={this.props.id}
          value={this.props.value}
          id={this.props.id}
        />
        <label htmlFor={this.props.id} /> {this.props.text}
      </div>
    );
  }
}

export default Checkbox;
