import React from "react";
import classes from "./RadioOptionRP.css";

const RadioOptionRP = props => {
  const id_radio = props.value + "-" + Math.random();
  return (
    <div className={classes.RadioOptionRP}>
      <input
        id={id_radio}
        type="radio"
        value={props.value}
        selected={props.selected}
        checked={props.selected === props.value}
        onChange={props.onChange}
      />
      <label className={classes.RadioLabel} htmlFor={id_radio}>
        {props.label}
      </label>
    </div>
  );
};

export default RadioOptionRP;
