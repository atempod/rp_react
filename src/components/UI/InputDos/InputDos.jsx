import React from "react";
import classes from "./InputDos.css";

function isInvalid({ valid, touched, shouldValidate }) {
  console.log("================== valid == ", valid);

  return !valid && shouldValidate && touched;
}

const InputDos = props => {
  const inputType = props.type || "text";
  const cls = [classes.InputDos];
  const htmlFor = `${inputType}-${Math.random()}`;

  //   console.log("================== is Invalid == ", isInvalid(props));
  if (isInvalid(props)) {
    // console.log("================== is Invalid == ", isInvalid(props));
    cls.push(classes.invalid);
  }

  return (
    <div className={cls.join(" ")}>
      <label htmlFor={htmlFor}>{props.label}</label>
      <input
        type={inputType}
        id={htmlFor}
        value={props.value}
        onChange={props.onChange}
      />

      {isInvalid(props) ? (
        <span>{props.errorMessage || "Введите верное значение"}</span>
      ) : null}
    </div>
  );
};

export default InputDos;
