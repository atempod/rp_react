import React from "react";
import classes from "./ButtonRP.css";

const ButtonRP = props => {
  let cls = [];
  if (!props.disabled) {
    cls = [classes.ButtonRP, classes[props.buttonStyle]];
  } else {
    cls = [classes.ButtonRPDisabled];
  }

  // console.log("props.disabled", props.disabled);
  return (
    <div className={cls.join(" ")}>
      <div onClick={props.onClick} disabled={props.disabled}>
        {props.children}
      </div>
    </div>
  );
};

export default ButtonRP;
