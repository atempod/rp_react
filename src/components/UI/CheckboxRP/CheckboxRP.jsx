import React from "react";
import classes from "./CheckboxRP.css";

const CheckboxRP = props => {
  return (
    <div className={classes.CheckboxRP}>
      <div className={classes.CheckBoxWrapper}>
        <input
          id="checkbox_id"
          type="checkbox"
          checked={props.checked}
          onChange={props.onChange}
        />
      </div>
      <label className={classes.CheckboxLabel} htmlFor="checkbox_id">
        {props.children}
      </label>
    </div>
  );
};

export default CheckboxRP;
