import React, { Component } from "react";
import classes from "./ModalPopup.css";
import Backdrop from "../Backdrop/Backdrop";
import InModalContent from "../../UI/InModalContent/InModalContent";

class ModalPopup extends Component {
  // state = {  }
  render() {
    const clsFrame = [classes.ModalFrame];
    const clsModal = [classes.ModalPopup];

    if (!this.props.isOpen) {
      clsFrame.push(classes.close);
      clsModal.push(classes.close);
    }
    console.log("type =", this.props.type);

    return (
      <React.Fragment>
        <div className={clsFrame.join(" ")}>
          {this.props.isOpen ? <Backdrop onClick={this.props.onClose} /> : null}
          <div className={clsModal.join(" ")}>
            <InModalContent type={this.props.type} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ModalPopup;
