import React, { Component } from "react";
import classes from "./InModalContent.css";
import LoginForm from "../../LoginForm/LoginForm";
import RegisterForm from "../../RegisterForm/RegisterForm";

class InModalContent extends Component {
  render() {
    const clsFrame = [classes.ModalFrame];
    const clsModal = [classes.InModalContent];

    if (!this.props.isOpen) {
      clsFrame.push(classes.close);
      clsModal.push(classes.close);
    }
    console.log("type =", this.props.type);

    let TagName = null;
    const type = this.props.type;
    switch (type) {
      case "loginModal":
        TagName = LoginForm;
        break;
      case "registerModal":
        TagName = RegisterForm;
        break;
      default:
        TagName = LoginForm;
        break;
    }
    console.log("TagName =", TagName);

    return <TagName />;
  }
}

export default InModalContent;
