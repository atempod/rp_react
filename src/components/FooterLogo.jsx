import React from "react";
import { Link } from "react-router-dom";
import classes from "./Footer.css";
import { Button } from "reactstrap";
import footerLogoImg from "../assets/images/footer-logo.png";
import ButtonRP from "./UI/ButtonRP/ButtonRP";

const FooterLogo = () => {
  return (
    <React.Fragment>
      <Link to="/">
        <div>
          <img
            src={footerLogoImg}
            alt="RedPuzzle Logo"
            className="position-relative"
          />
        </div>
        <p className={classes.FooterLogoNote}>
          Социальный фрагмент благополучия
        </p>
      </Link>
      <ButtonRP color="rpRed">Разместить вакансию</ButtonRP>
    </React.Fragment>
  );
};

export default FooterLogo;
