import React from "react";
import classes from "./NavbarBar.css";
import NavbarBlock from "./NavbarBlock";
import NavAbout from "./NavAbout";

import { Container, Navbar } from "reactstrap";

const NavbarBar = () => {
  return (
    <Navbar sticky="top" className={classes.NavbarBar}>
      <Container>
        <NavbarBlock />
        <NavAbout />
      </Container>
    </Navbar>
  );
};

export default NavbarBar;
