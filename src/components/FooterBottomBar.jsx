import React from "react";
import { Container, Row, Col } from "reactstrap";
import classes from "./FooterBottomBar.css";

class FooterBottomBar extends React.Component {
  state = { vacancies: 9, resume: 13, companies: 20 };
  render() {
    return (
      <footer className={"bg-rpBlueDark"}>
        <Container>
          <Row>
            <Col xs="6">
              <span className={classes.BottomFooterSpanLeft}>
                © 2017 ООО “РЕДПАЗЛ” Сервис для трудовых мигрантов
              </span>
            </Col>
            <Col xs="6" className={classes.BottomFooterBar}>
              <span className={classes.BottomFooterSpanRight}>
                Сегодня на сайте {this.state.vacancies} вакансий,
                {this.state.resume} резюме, {this.state.companies} компаний
              </span>
            </Col>
          </Row>
        </Container>
      </footer>
    );
  }
}

export default FooterBottomBar;
