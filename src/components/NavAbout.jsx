import React from "react";
import classes from "./NavAbout.css";
import sprite from "../assets/images/sprite.png";

const NavAbout = () => {
  return (
    <div className={classes.NavAbout}>
      <ul>
        <li>
          <a href="/">
            <i src={sprite} />
            <i />
            <span>О проекте</span>
          </a>
        </li>
      </ul>
    </div>
  );
};

export default NavAbout;
