import React from "react";
import { Link } from "react-router-dom";
import classes from "./HeaderLogo.css";
import headerLogoImg from "../assets/images/logo.png";

const HeaderLogo = () => {
  return (
    <React.Fragment>
      <Link to="/">
        <div>
          <img
            src={headerLogoImg}
            alt="RedPuzzle Logo"
            className={classes.HeaderLogoNote}
          />
        </div>
        <p className={classes.HeaderLogoNote}>Российский Красный Крест</p>
      </Link>
    </React.Fragment>
  );
};

export default HeaderLogo;
