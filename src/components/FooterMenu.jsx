import React, { Component } from "react";
import classes from "./FooterMenu.css";

class FooterMenu extends Component {
  state = {};
  render() {
    let cls_FooterMenu = [classes.FooterMenu];
    const footer_bootstr_cls = "text-rpBlueLight";
    cls_FooterMenu.push(footer_bootstr_cls);

    return (
      <div>
        <div className={cls_FooterMenu.join(" ")}>
          <div className="row">
            <div className="col-sm-4">
              <div className={classes.FooterMenuTitle}>Соискателям</div>
              <ul>
                <li>
                  <a href="/agency/vacancy-categories/">Каталог вакансий </a>
                </li>
                <li>
                  <a href="/documents/soglashenie-soiskatelia/">
                    Соглашение об оказании услуг{" "}
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-sm-4">
              <div className={classes.FooterMenuTitle}>Работадателям</div>
              <ul>
                <li>
                  <a href="/agency/resume-categories/">Каталог резюме </a>
                </li>
                <li>
                  <a href="/">Условия использования сайта </a>
                </li>
                <li>
                  <a href="/documents/pravila-razmeshcheniia-vakansii/">
                    Правила размещения вакансий{" "}
                  </a>
                </li>
              </ul>
            </div>
            <div>
              <div className={classes.FooterMenuTitle}>Информация</div>
              <ul>
                <li>
                  <a href="/documents/personal-data/">
                    Защита персональных данных{" "}
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FooterMenu;
