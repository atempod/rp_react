import React, { Component } from "react";
import "./Header.scss";
import LoginButton from "./UI/LoginButton/LoginButton";
import RegisterButton from "./UI/RegisterButton/RegisterButton";
// import ButtonRP from "./UI/ButtonRP/ButtonRP";
import ModalPopup from "./UI/ModalPopup/ModalPopup";
// import InputRP from "./UI/InputRP/InputRP";
import TopSearch from "./TopSearch/TopSearch";

import {
  Container,
  Row,
  Col,
  // Form,
  // Input,
  Navbar
  // Button,
  // NavbarBrand
} from "reactstrap";
import HeaderLogo from "./HeaderLogo";

class Header extends Component {
  state = {
    modalOpen: false,
    type: null
  };

  onOpenLoginHandler = () => {
    this.setState({ modalOpen: true, type: "loginModal" });
  };

  onOpenRegisterHandler = () => {
    this.setState({ modalOpen: true, type: "registerModal" });
  };

  modalCloseHandler = () => {
    this.setState({ modalOpen: false, type: null });
  };

  render() {
    console.log(this.state);

    return (
      <header>
        <ModalPopup
          isOpen={this.state.modalOpen}
          type={this.state.type}
          onClose={this.modalCloseHandler}
        />
        <Navbar
          color="light"
          light
          expand="xs"
          className="border-bottom border-gray bg-white"
          style={{ height: 107 }}
        >
          <Container>
            <Row
              noGutters
              className="position-relative w-100 align-items-center"
            >
              <Col>
                <HeaderLogo />
              </Col>

              <Col className="d-none d-lg-flex justify-content-end">
                <TopSearch />
                {/* <Form inline>
                  <Input
                    type="search"
                    className="mr-3"
                    placeholder="Search React Courses"
                  />

                  <ButtonRP type="submit" buttonStyle="btn--header" outline>
                    <span>Найти</span>
                  </ButtonRP>
                </Form> */}
              </Col>

              <Col className="d-flex justify-content-xs-start justify-content-lg-end">
                <LoginButton onLoginOpen={this.onOpenLoginHandler}>
                  Войти
                </LoginButton>
                <RegisterButton onRegisterOpen={this.onOpenRegisterHandler}>
                  Регистрация
                </RegisterButton>
              </Col>
            </Row>
          </Container>
        </Navbar>
      </header>
    );
  }
}
export default Header;
