import React, { Component } from "react";
import classes from "./LoginForm.css";
import ButtonRP from "../UI/ButtonRP/ButtonRP";
import InputRP from "../UI/InputRP/InputRP";
import CheckboxRP from "../UI/CheckboxRP/CheckboxRP";
import MdCheckboxOutline from "react-ionicons/lib/MdCheckboxOutline";
import is from "is_js";
import axios from "axios";

class LoginForm extends Component {
  state = {
    isFormValid: false,
    inputStyle: "registerModal",
    toRememberChecked: false,
    toRememberCheckBoxText: "Запомнить меня",

    formControls: {
      email: {
        value: "",
        type: "email",
        label: "Email",
        inputPlaceholder: "Email",
        errorMessage: "Введите корректный email",
        valid: false,
        touched: false,
        validation: {
          required: true,
          email: true
        }
      },
      password: {
        value: "",
        type: "password",
        label: "Пароль",
        inputPlaceholder: "Пароль",
        errorMessage: "Введите корректный пароль",
        valid: false,
        touched: false,
        validation: {
          required: true,
          minLength: 6
        }
      }
    }
  };

  validateControl(value, validation) {
    if (!validation) {
      return true;
    }

    let isValid = true;

    if (validation.required) {
      isValid = value.trim() !== "" && isValid;
    }

    if (validation.minLength) {
      isValid = value.trim().length >= validation.minLength && isValid;
    }

    if (validation.email) {
      isValid = is.email(value) && isValid;
    }

    return isValid;
  }

  loginHandler = async () => {
    if (this.state.isFormValid) {
      console.log("loginHandler works");
      const authData = {
        email: this.state.formControls.email.value,
        password: this.state.formControls.password.value,
        returnSecureToken: true
      };
      console.log("login Handler authDate=", authData);
      try {
        const response = await axios.post(
          "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyBRj1R0UEHzbzdDaOOQIhjqWUvDsusN4Mo",
          authData
        );
        console.log("response.data === ", response.data);
      } catch (e) {
        console.log("error ===> ", e);
      }
    }
  };

  loginHandler_www = async () => {
    const authData = {
      email: this.state.formControls.email.value,
      password: this.state.formControls.password.value,
      returnSecureToken: true
    };
    console.log("login Handler", authData);
    try {
      const response = await axios.post(
        "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyBRj1R0UEHzbzdDaOOQIhjqWUvDsusN4Mo",
        authData
      );

      console.log(response.data);
    } catch (e) {
      console.log(e);
    }
  };

  onChangeHandler = (event, controlName) => {
    const formControls = { ...this.state.formControls };
    const control = { ...formControls[controlName] };

    control.value = event.target.value;
    control.touched = true;
    control.valid = this.validateControl(control.value, control.validation);

    formControls[controlName] = control;

    let isFormValid = true;

    Object.keys(formControls).forEach(name => {
      isFormValid = formControls[name].valid && isFormValid;
    });

    this.setState({
      formControls,
      isFormValid
    });
  };

  submitHandler = event => {
    event.proventDefault();
  };

  renderInputs() {
    const inputStyle = this.state.inputStyle;
    return Object.keys(this.state.formControls).map((controlName, index) => {
      const control = this.state.formControls[controlName];
      return (
        <InputRP
          inputStyle={inputStyle}
          key={controlName + index}
          value={control.value}
          type={control.type}
          label={control.label}
          inputPlaceholder={control.inputPlaceholder}
          errorMessage={control.errorMessage}
          valid={control.valid}
          touched={control.touched}
          shouldValidate={!!control.validation}
          onChange={event => this.onChangeHandler(event, controlName)}
        />
      );
    });
  }

  toggleЕoRememberCheckBoxHandler = () => {
    const state = { ...this.state };
    const toRememberChecked = state["toRememberChecked"];

    this.setState(() => ({
      toRememberChecked: !toRememberChecked
    }));
  };

  render() {
    const inputMode = "login--input";
    const cls = [classes.LoginForm, inputMode];

    if (!this.props.isOpen) {
      cls.push(classes.close);
    }
    console.log(cls);

    return (
      <div className={cls.join(" ")}>
        <div>
          <h3>Войти</h3>
          <form onSubmit={this.submitHandler}>
            {this.renderInputs()}

            <div className={classes.rememberMe}>
              <CheckboxRP
                checked={this.state.toRememberChecked}
                onChange={this.toggleЕoRememberCheckBoxHandler}
              >
                {this.state.toRememberCheckBoxText}
                <MdCheckboxOutline
                  onClick={() => alert("Hi!")}
                  fontSize="20px"
                  // color="#43853d"
                  color="red"
                />
              </CheckboxRP>
            </div>

            <div className={classes.centered}>
              <ButtonRP
                type="success"
                buttonStyle="btn--3d"
                onClick={this.loginHandler}
                disabled={!this.state.isFormValid}
              >
                <span>Войти на сайт</span>
              </ButtonRP>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default LoginForm;
