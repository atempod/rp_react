import React, { Component } from "react";
import ButtonRP from "../UI/ButtonRP/ButtonRP";
import InputRP from "../UI/InputRP/InputRP";

class TopSearch extends Component {
  state = {};

  submitHandler = event => {
    event.preventDefault();
  };

  render() {
    return (
      <div>
        <form onSubmit={this.submitHandler}>
          {/* <Input
                    type="search"
                    className="mr-3"
                    placeholder="Search React Courses"
                  /> */}
          <InputRP
            type="text"
            // label="Top Search"
            inputStyle="topSearch"
            inputPlaceholder="Top Search!!!"
          />
          <ButtonRP type="submit" buttonStyle="btn--header" outline>
            <span>Найти</span>
          </ButtonRP>
        </form>
      </div>
    );
  }
}

export default TopSearch;
