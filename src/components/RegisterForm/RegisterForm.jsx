import React, { Component } from "react";
import classes from "./RegisterForm.css";
import ButtonRP from "../UI/ButtonRP/ButtonRP";
import InputRP from "../UI/InputRP/InputRP";
import CheckboxRP from "../UI/CheckboxRP/CheckboxRP";
import RadioOptionRP from "../UI/RadioOptionRP/RadioOptionRP";
import is from "is_js";
import axios from "axios";

class RegisterForm extends Component {
  state = {
    isFormValid: false,
    areControlsValid: false,
    isAgreeChecked: false,
    agreeCheckBoxText:
      "Нажимая кнопку «Зарегистрироваться», я принимаю условия обработки персональных данных «Соискателя» или «Работодателя» и «Политики безопасности персональных данных».",
    inputStyle: "registerModal",
    formControls: {
      name: {
        value: "",
        type: "text",
        label: "Имя",
        inputPlaceholder: "Имя",
        errorMessage: "Введите корректное имя",
        valid: false,
        touched: false,
        validation: {
          required: true,
          minLength: 6
        }
      },
      email: {
        value: "",
        type: "email",
        label: "Email",
        inputPlaceholder: "Email",
        errorMessage: "Введите корректный email",
        valid: false,
        touched: false,
        validation: {
          required: true,
          email: true
        }
      },
      phone: {
        value: "",
        type: "text",
        label: "Телефон",
        inputPlaceholder: "Телефон",
        errorMessage: "Введите корректный телефон",
        valid: false,
        touched: false,
        validation: {
          required: true,
          phoneNumber: true
        }
      },
      password: {
        value: "",
        type: "password",
        label: "Пароль",
        inputPlaceholder: "Пароль",
        errorMessage: "Введите корректный пароль",
        valid: false,
        touched: false,
        validation: {
          required: true,
          minLength: 6
        }
      },
      password2: {
        value: "",
        type: "password",
        label: "Повторите пароль",
        inputPlaceholder: "Повторите пароль",
        errorMessage: "Корректно повторите пароль",
        valid: false,
        touched: false,
        validation: {
          required: true,
          passwordIdentity: true
        }
      }
    },
    selectedUserType: "jobseeker",
    userType: {
      jobseeker: {
        label: "Я соискатель"
      },
      employer: {
        label: "Я работодатель"
      },
      agent: {
        label: "Я агент"
      }
    }
  };

  validateControl(value, validation) {
    if (!validation) {
      return true;
    }

    let isValid = true;

    if (validation.required) {
      isValid = value.trim() !== "" && isValid;
    }

    if (validation.minLength) {
      isValid = value.trim().length >= validation.minLength && isValid;
    }

    if (validation.email) {
      isValid = is.email(value) && isValid;
    }

    if (validation.phoneNumber) {
      const regExpPattern = /^(\+?|\(\+?)[\d ()-]+$/;
      is.setRegexp(regExpPattern, "eppPhone");
      isValid = is.eppPhone(value) && isValid;
    }

    if (validation.passwordIdentity) {
      isValid = value === this.state.formControls.password.value && isValid;
    }

    return isValid;
  }

  registerHandler = async () => {
    if (this.state.areControlsValid) {
      console.log("registerHandler works");
      const authData = {
        email: this.state.formControls.email.value,
        password: this.state.formControls.password.value,
        returnSecureToken: true
      };

      try {
        const response = await axios.post(
          "https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyBRj1R0UEHzbzdDaOOQIhjqWUvDsusN4Mo",
          authData
        );

        console.log(response.data);
      } catch (e) {
        console.log(e);
      }
      console.log("register Handler == authData", authData);
    }
  };

  submitHandler = event => {
    event.proventDefault();
  };

  renderUserTypeRadioGroup() {
    return Object.keys(this.state.userType).map((userTypeName, index) => {
      const userTypeControl = this.state.userType[userTypeName];
      return (
        <RadioOptionRP
          key={userTypeName + index}
          label={userTypeControl.label}
          value={userTypeName}
          onChange={this.onChangeRadioHandler}
          selected={this.state.selectedUserType}
        />
      );
    });
  }

  renderInputs() {
    const inputStyle = this.state.inputStyle;
    return Object.keys(this.state.formControls).map((controlName, index) => {
      const control = this.state.formControls[controlName];
      return (
        <InputRP
          inputStyle={inputStyle}
          key={controlName + index}
          value={control.value}
          type={control.type}
          label={control.label}
          inputPlaceholder={control.inputPlaceholder}
          errorMessage={control.errorMessage}
          valid={control.valid}
          touched={control.touched}
          shouldValidate={!!control.validation}
          onChange={event => this.onChangeTextHandler(event, controlName)}
        />
      );
    });
  }

  onChangeTextHandler = (event, controlName) => {
    const formControls = { ...this.state.formControls };
    const control = { ...formControls[controlName] };

    control.value = event.target.value;
    control.touched = true;
    control.valid = this.validateControl(control.value, control.validation);

    formControls[controlName] = control;

    let areControlsValid = true;

    Object.keys(formControls).forEach(name => {
      areControlsValid = formControls[name].valid && areControlsValid;
    });

    this.setState(() => ({
      formControls,
      areControlsValid
    }));
  };

  toggleCheckboxHandler = () => {
    const state = { ...this.state };
    const isAgreeChecked = state["isAgreeChecked"];

    this.setState(() => ({
      isAgreeChecked: !isAgreeChecked
    }));
  };

  onChangeRadioHandler = ({ target: { value } }) => {
    this.setState(() => ({
      selectedUserType: value
    }));
  };

  render() {
    const isFormValid =
      this.state.areControlsValid && this.state.isAgreeChecked;
    const cls = [classes.RegisterForm];

    if (!this.props.isOpen) {
      cls.push(classes.close);
    }

    return (
      <div className={cls.join(" ")}>
        <div>
          <h3>Регистрация</h3>
          <form onSubmit={this.handleFormSubmit}>
            <div className={classes.userType}>
              {this.renderUserTypeRadioGroup()}
            </div>

            {this.renderInputs()}

            <CheckboxRP
              checked={this.state.isAgreeChecked}
              onChange={this.toggleCheckboxHandler}
            >
              {this.state.agreeCheckBoxText}
            </CheckboxRP>

            <div className={classes.centered}>
              <ButtonRP
                type="success"
                buttonStyle="btn--3d"
                onClick={this.registerHandler}
                disabled={!isFormValid}
              >
                <span>Зарегистрироваться</span>
              </ButtonRP>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default RegisterForm;
