import React from "react";
import classes from "./Footer.css";
import footerLogo from "../assets/images/footer-logo.png";
import FooterMenu from "./FooterMenu";
import ButtonRP from "./UI/ButtonRP/ButtonRP";

import {
  Container,
  Row,
  Col
  // Button
} from "reactstrap";

let cls = [classes.footer];
const footer_bootstr_cls = "bg-rpBlue";
cls.push(footer_bootstr_cls);

const Footer = () => (
  <footer className={cls.join(" ")}>
    <Container>
      <Row>
        <Col sm="4" md="3">
          <div className={classes.logoFooterImg}>
            <a href="/">
              <img src={footerLogo} alt="RedPuzzle Logo" />
              <span className={classes.logoFooterNote}>
                Социальный фрагмент благополучия
              </span>
            </a>
          </div>
          <ButtonRP buttonStyle="btn--3d">
            <span>Разместить резюме</span>
          </ButtonRP>
        </Col>
        <Col sm="8" md="9">
          <FooterMenu />
        </Col>
      </Row>
    </Container>
  </footer>
);

export default Footer;
