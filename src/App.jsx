import React, { Fragment } from "react";
import { Route, Switch } from "react-router-dom";
// import axios from "axios";

import Header from "./components/Header";
import Footer from "./components/Footer";
import FooterBottomBar from "./components/FooterBottomBar";
import HeaderTop from "./components/HeaderTop";
import NavbarBar from "./components/NavbarBar";
import MainInitial from "./containers/MainInitial";
import MainHome from "./containers/MainHome";
import Page404 from "./containers/Page404";
import FilterableVacancyList from "./containers/FilterableVacancyList";

const App = props => {
  console.log("props in App", props.vacancies);
  return (
    <Fragment>
      <HeaderTop />
      <Header />
      <NavbarBar />
      <Switch>
        <Route path="/init" component={MainInitial} />
        <Route path="/one-more" component={MainHome} />
        <Route
          path="/filtered-vacancies"
          component={() => (
            <FilterableVacancyList vacancies={props.vacancies} />
          )}
        />
        <Route path="/" exact component={MainHome} />
        <Route path="/" component={Page404} />
      </Switch>
      <Footer />
      <FooterBottomBar />
    </Fragment>
  );
};

export default App;
