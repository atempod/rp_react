// import "bootstrap/dist/css/bootstrap.min.css";
// import "bootstrap/scss/bootstrap.scss";
import "./assets/scss/mytheme.scss";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

var VACANCIES = [
  {
    id: "1",
    categoryId: "IT",
    speciality: "Web developer",
    salary: "10000",
    company: "BSNL",
    city: "Chennai",
    active: true
  },
  {
    id: "2",
    categoryId: "IT",
    speciality: "React developer",
    salary: "20000",
    company: "TVS",
    city: "Bangalor",
    active: true
  },
  {
    id: "3",
    categoryId: "IT",
    speciality: "JS programmer",
    salary: "30000",
    company: "Honda",
    city: "Tiru",
    active: true
  },
  {
    id: "4",
    categoryId: "Transport",
    speciality: "Driver",
    salary: "20000",
    company: "Honda",
    city: "Tiru",
    active: true
  },
  {
    id: "5",
    categoryId: "Transport",
    speciality: "Director",
    salary: "250000",
    company: "Doca",
    city: "Mumbai",
    active: true
  },
  {
    id: "6",
    categoryId: "Trading",
    speciality: "Cashier",
    salary: "10000",
    company: "Doca",
    city: "Mumbai",
    active: true
  }
];

const app = (
  <BrowserRouter>
    <App vacancies={VACANCIES} />
  </BrowserRouter>
);

ReactDOM.render(app, document.getElementById("root"));
serviceWorker.register();

// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import * as serviceWorker from './serviceWorker';

// ReactDOM.render(<App />, document.getElementById('root'));

// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
