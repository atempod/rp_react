import React, { Component } from "react";
import { Container } from "reactstrap";

class MainHome extends Component {
  state = {};
  render() {
    return (
      <main className="my-5 py-5">
        <Container className="px-0">
          <h1>Main Home Page</h1>
        </Container>
      </main>
    );
  }
}

export default MainHome;
