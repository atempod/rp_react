import React from "react";

const VacancyRow = props => {
  return (
    <tr>
      {Object.keys(props.vacancy).map(key => (
        <td value={key}>{props.vacancy[key]}</td>
      ))}
    </tr>
  );
};

export default VacancyRow;
