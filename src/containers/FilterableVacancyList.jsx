// import React from "react";
import React, { Component } from "react";
import VacancyTable from "./VacancyTable";
import InlineSearchBar from "../components/InlineSearchBar";
import { Container } from "reactstrap";

class FilterableVacancyList extends Component {
  state = {};

  render(props) {
    return (
      <main className="my-3 py-3">
        <Container className="px-0">
          <h1>============ FilterableVacancyList =================</h1>
          <InlineSearchBar />
          <VacancyTable vacancies={this.props.vacancies} />
        </Container>
      </main>
    );
  }
}

export default FilterableVacancyList;

// const FilterableVacancyList = props => {
//   console.log("props in Filtered Vacancies", props.vacancies);

//   return (
//     <main className="my-3 py-3">
//       <Container className="px-0">
//         <h1>============ FilterableVacancyList =================</h1>
//         <InlineSearchBar />
//         <VacancyTable vacancies={props.vacancies} />
//       </Container>
//     </main>
//   );
// };

// export default FilterableVacancyList;
