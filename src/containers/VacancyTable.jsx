import React from "react";
import { Table } from "reactstrap";
import VacancyRow from "./VacancyRow";

const VacancyTable = props => {
  console.log("props", props, props.vacancies[0]);
  const vacancies = props.vacancies;

  return (
    <React.Fragment>
      <h2>VacancyTable</h2>

      <div>
        {vacancies.map((vacancy, index) => (
          <Table>
            {index === 0 ? (
              <thead>
                <tr>
                  {Object.keys(vacancy).map(key => (
                    <th value={key}>{key}</th>
                  ))}
                </tr>
              </thead>
            ) : null}
            <tbody>
              <VacancyRow vacancy={vacancy} />
            </tbody>
          </Table>
        ))}
      </div>
    </React.Fragment>
  );
};

export default VacancyTable;

//   textAlign="center" verticalAlign="middle"
