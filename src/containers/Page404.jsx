import React, { Component } from "react";
import { Container } from "reactstrap";

class Page404 extends Component {
  state = {};
  render() {
    return (
      <main className="my-5 py-5">
        <Container className="px-0">
          <h1>============ Page 404 =================</h1>
        </Container>
      </main>
    );
  }
}

export default Page404;
